<?php include_once 'inc/header.php';?>

<div class="pure-g">

	<div class="pure-u-1 pure-u-md-2-3">
		<div class="pane">
			<div class="block">
				<h1>A friendly, local property management service for West Midlands apartment blocks and estates.</h1>
				<div class="row">
					<p>Longlease Property Management was founded in 2009 to provide a friendly local service that delivered cost-effective, responsive and high quality block and estate management.</p>
				</div>

				<div class="row">
					<img class="pure-img align_left" src="/images/about-1.jpg" alt="" />
					<h2>A focus on the West Midlands means we provide a local, hands-on and committed property management service.</h2>
					<p>We understand the difficulties caused by managing properties over a wide geographic area. When a property manager covers a wide area, the distances involved means response times are too long and the standard of service falls because they are overstretched.</p>
					<p>A key part our business plan was only to manage properties in a specific area. By focusing on the West Midlands, every resident, landlord, shareholder and developer receives a high standard of service.</p>
					<p>This commitment gives you the peace of mind that you are working with a managing agent who will look after your property as if they lived there themselves.</p>
				</div>

				<div class="row">
					<img class="pure-img align_right" src="/images/about-2.jpg" alt="" />
					<h2>Best practice property management services</h2>
					<p>We use the <a href="http://www.rics.org/uk/knowledge/professional-guidance/codes-of-practice">RICS code of practice</a> as the basis for our property management services. RICS (the Royal Institution of Chartered Surveyors) represents everything professional and ethical in land, property and construction and we are proud to uphold its standards.</p>
				</div>

				<div class="row">
					<img class="pure-img align_left" src="/images/about-3.jpg" alt="" />
					<h2>A refreshing enthusiasm for property management</h2>
					<p>Longlease Property Management is large enough to have the resources to offer a thorough, professional and cost-effective service. It is also small enough to provide a local, hands-on service that is genuinely committed to supporting your property and the people who live there.</p>
					<p>If you would like to discuss your block or estate management requirements, please <a href="/contact">get in touch</a>. We hope you will be pleasantly surprised by the enthusiasm we have for property management and delivering a service that will add value to your home and property.</p>
				</div>
			</div>
		</div>
	</div>

	<?php include_once 'inc/sidebar.php';?>

</div>

<?php include_once 'inc/footer.php';?>