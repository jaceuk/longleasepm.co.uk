<?php include_once 'inc/config.php';?>

<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->

<head>

	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Longlease Property Management Malvern</title>

	<link rel="shortcut icon" href="/images/favicon.ico">

	<link rel="stylesheet" href="/css/pure-min.css">
	<link rel="stylesheet" href="/css/style.css">

	<link href='//fonts.googleapis.com/css?family=Open+Sans:700,600,400,300' rel='stylesheet' type='text/css'>

	<!--[if IE]>
		<link href='//fonts.googleapis.com/css?family=Open+Sans:700' rel='stylesheet' type='text/css'>
		<link href='//fonts.googleapis.com/css?family=Open+Sans:600' rel='stylesheet' type='text/css'>
		<link href='//fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
		<link href='//fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
	<![endif]-->

	<!--[if lte IE 8]>
	    <style>
	    	body {
	    		min-width:1024px;
	    	}
	    </style>
	<![endif]-->

	<!--[if gt IE 8]><!-->
	    <link rel="stylesheet" href="/css/grids-responsive-min.css">
	<!--<![endif]-->

	<?php if (VERSION == 'live') { ?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-66183168-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<?php } ?>

</head>

<body>

<?php
if (VERSION == 'local') {
	echo '<p>This site is being run on a local dev machine.</p>';
}
?>

	<header class="clearfix pane">
		<a id="logo_link" href="/"><img class="pure-img" src="images/logo.png" alt="Longlease Property Management logo" /></a>

		<nav id="desktop">
			<a class="pure-button<?php if (PAGE == 'index') { echo ' active'; } ?>" href="/">Home</a>
			<a class="pure-button<?php if (PAGE == 'about') { echo ' active'; } ?>" href="/about">About</a>
			<a class="pure-button<?php if (PAGE == 'services') { echo ' active'; } ?>" href="/services">Services</a>
			<a class="pure-button<?php if (PAGE == 'help') { echo ' active'; } ?>" href="/help">How We Help You</a>
			<a class="pure-button<?php if (PAGE == 'contact') { echo ' active'; } ?>" href="/contact">Contact</a>
		</nav>

		<ul id="mobile_link">
			<li><a href="javascript: void(0);">Menu <span class="menu_icon"></span></a></li>
		</ul>

		<ul id="dropdown_mobile">
			<a href="/">Home</a>
			<a href="/about">About</a>
			<a href="/services">Services</a>
			<a href="/help">How We Help You</a>
			<a href="/contact">Contact</a>
		</ul>

	</header>
