
	<footer>
		<p class="copyright">&copy; 2015 Longlease Property Management | <a href="/privacy-policy">Privacy Policy</a> | </a><a href="http://www.revolvedigital.co.uk">Website design and development by Revolve Digital Solutions</a></p>
	</footer>

	<!-- Grab Google CDN's jQuery with local fallback, 1.x.x is needed to support ie8 -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>
	if (!window.jQuery) {
	    document.write('<script src="js/jquery_min.js"><\/script>');
	}
	</script>

	<!-- this is where we put our custom functions -->
	<script src="js/functions.js"></script>

</body>
</html>