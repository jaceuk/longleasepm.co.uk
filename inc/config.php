<?php

// turn google analytics off when being run locally
if (file_exists('c:\wamp')) {
	define('VERSION', 'local');
} else {
	define('VERSION', 'live');
}

define('PAGE', str_replace('.php', '', basename($_SERVER['PHP_SELF'])));

?>