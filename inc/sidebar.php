<div id="sidebar" class="pure-u-1 pure-u-md-1-3">
	<div class="pane">
		<div class="block">
			<h2>Contact Details</h2>
			<p><strong>Tel:</strong> 01789 333 464</p>
			<p><strong>Email:</strong> <a href="mailto:info@longleasepm.co.uk">info@longleasepm.co.uk</a></p>
		</div>
	</div>

	<div class="pane">
		<div class="block">
			<h2>Why choose Longlease Management?</h2>
			<p>We offer the perfect combination of extensive block management experience and committed personal service.</p>
			<p>We support resident management companies, right to manage companies, property developers and freehold investors in Worcestershire by providing apartment management services that make managing your block easy.</p>
			<ul>
				<li><a href="/about">Find out more about Longlease</a></li>
				<li><a href="/services">Find out more about our apartment management services</a></li>
				<li><a href="/help">Find out more about how we can help you</a></li>
			</ul>
		</div>
	</div>

    <div class="pane">
        <div class="block">
            <div class="prs-logo">
                <img src="images/prs-logo.png" alt="" />
            </div>
        </div>
    </div>
</div>