<?php include_once 'inc/header.php';?>

<div class="pure-g">

	<div class="pure-u-1 pure-u-md-2-3">
		<div class="pane">
			<div class="block">

				<img class="pure-img" src="/images/home.jpg" alt="" />

				<h1>A property management company that delivers the service you'd like to receive.</h1>
				<p>Longlease Property Management is based in Warwickshire and Worcestershire and we only manage apartment blocks and estates in the West Midlands. Our focus means we provide a dedicated and hands-on block management service that's designed to enhance your property.</p>
				<p>Help is at hand if you are:</p>
				<ul>
					<li>a leaseholder experiencing poor service from your current managing agent</li>
					<li>a leaseholder struggling to keep up with the demands of managing your property</li>
					<li>a developer looking for an agent who will increase your reputation for quality after-service</li>
					<li>a freehold investor wanting to ensure your property is well-maintained</li>
				</ul>
				<p><a href="/contact">Get in touch today!</a></p>
                <h2>Why choose Longlease Management?</h2>
                <p>We offer the perfect combination of extensive block and estate management experience and a committed local and personal service.</p>
                <p>We support resident management companies, right to manage companies, property developers and freehold investors across the West Midlands by providing services that make managing your block or estate easy.</p>
			</div>
		</div>
	</div>

	<?php include_once 'inc/sidebar.php';?>

</div>

<?php include_once 'inc/footer.php';?>