<?php include_once 'inc/header.php';?>

<div class="pure-g">

	<div class="pure-u-1 pure-u-md-2-3">
		<div class="pane">
			<div class="block">
				<h1>How We Help You</h1>

				<div class="row">
					<img class="pure-img align_left" src="/images/help-1.jpg" alt="" />
					<p>If any of the following scenarios sound familiar, please give us a call. We would be happy to discuss how our property management services could help you. We provide a bespoke and personal service that is effective, efficient, tactful, professional and friendly</p>

                    <h2>Resident Management Company Directors</h2>
					<p>Have you had enough of managing your block of flats? Do you worry about maintenance chores, the accounts, the legislation, health and safety regulations, chasing neighbours for unpaid service charges, having to organise contractors?Our property management services can relieve you of all these worries and more. We can give you the freedom to enjoy your home again.</p>
				</div>

				<div class="row">
					<h2>Property Developer / House Builder</h2>
					<p>Do you find you're too busy concentrating on the build phase to worry about the sales phase? Do you spend too much time managing your built properties and not enough on new building projects?</p>
					<p>We can work alongside you to ensure that you have everything in place when your property goes on the market. We can also take care of ongoing property management, ensuring that your properties have a reputation for good block management and enhancing your good name.</p>
				</div>

				<div class="row">
					<img class="pure-img align_right" src="/images/help-2.jpg" alt="" />
					<h2>Leaseholders, shareholders or residents</h2>
					<p>Are you tired of your current block manager? Are you being ignored or is your development looking tired due to lack of care? Do you feel your manager thinks you will just put up with poor service and do nothing? Has your landlord refused to change property management companies?</p>
					<p>Do not despair - you have rights and do not have to suffer poor block management standards. We can work with you to provide a solution that will give you back your pride in your home.</p>
				</div>

				<div class="row">
					<img class="pure-img align_left" src="/images/help-3.jpg" alt="" />
					<h2>Freehold Investors</h2>
					<p>Are you looking for a managing agent to look after your investments and keep you in touch with your portfolio? Do you want to feel confident that your leaseholders are being treated with courtesy and your investments are in good condition?</p>
					<p>We can give you peace of mind that your properties are being managed to the standards you would expect.</p>
				</div>

				<div class="row">
					<h2>Meet the Team</h2>
					<h3>Adam Barzey</h3>
                    <h3>David Hadrill</h3>
                    <h3>Victoria Beasley</h3>
                    <h3>Nikki Evis</h3>
                    <h3>Gill Chilcott</h3>
				</div>
			</div>
		</div>
	</div>

	<?php include_once 'inc/sidebar.php';?>

</div>

<?php include_once 'inc/footer.php';?>