<?php include_once 'inc/header.php';?>

<div class="pure-g">

	<div class="pure-u-1 pure-u-md-2-3">
		<div class="pane">
			<div class="block">
				<h1>Privacy Policy</h1>

                <div class="post-content">
                    <p>Longlease Property Management understands that your privacy is important to you and that you care about how
your personal data is used. We respect and value the privacy of all of our clients and will only collect and use
personal data in ways that are described here, and in a way that is consistent with our obligations and your
rights under the law.</p>
                    <p>
                        <strong>1. Information About Us</strong>
                    </p>
                    <p>TLG Service Charges Ltd T/A Longlease Property Management
                        <br> A limited Company registered in England under company number 09682580.
                        <br> Registered address: 6 Abbey Lane Court, Abbey Lane, Evesham, Worcs WR11 4BY
                        <br> VAT number: 289 5023 73
                        <br> Data Protection Officer: David Hadrill
                        <br> Email address: dataprotection@longleasepm.co.uk
                        <br> Telephone number: 01905 697979 Postal Address: As registered office
                        <br> Postal Address: As registered office</p>
                    <p>We are regulated by Royal Institution of Chartered Surveyors (RICS)</p>
                    <p>
                        <strong>2. What Does This Notice Cover?</strong>
                    </p>
                    <p>This Privacy Information explains how we use your personal data: how it is collected, how it is held, and how it
is processed. It also explains your rights under the law relating to your personal data.</p>
                    <p>
                        <strong>3. What is Personal Data?</strong>
                    </p>
                    <p>Personal data is defined by the General Data Protection Regulation (EU Regulation 2016/679) (the “GDPR”) as
‘any information relating to an identifiable person who can be directly or indirectly identified in particular by
reference to an identifier’.</p>
                    <p>Personal data is, in simpler terms, any information about you that enables you to be identified. Personal data
covers obvious information such as your name and contact details, but it also covers less obvious information
such as identification numbers, electronic location data, and other online identifiers.
                        <br> The personal data that we use is set out in Part 5, below.</p>
                    <p>
                        <strong>4. What Are My Rights?</strong>
                    </p>
                    <p>Under the GDPR, you have the following rights, which we will always work to uphold:</p>
                    <ul>
                        <li>The right to be informed about our collection and use of your personal data. This Privacy Notice should
tell you everything you need to know, but you can always contact us to find out more or to ask any
questions using the details in Part 11.</li>
                        <li>The right to access the personal data we hold about you. Part 10 will tell you how to do this.</li>
                        <li>The right to have your personal data rectified if any of your personal data held by us is inaccurate or
incomplete. Please contact us using the details in Part 11 to find out more.</li>
                        <li>The right to be forgotten, i.e. the right to ask us to delete or otherwise dispose of any of your personal
data that we have. Please contact us using the details in Part 11 to find out more.</li>
                        <li>The right to restrict (i.e. prevent) the processing of your personal data.</li>
                        <li>The right to object to us using your personal data for a particular purpose or purposes.</li>
                        <li>The right to data portability. This means that, if you have provided personal data to us directly, we are
using it with your consent or for the performance of a contract, and that data is processed using
automated means, you can ask us for a copy of that personal data to re-use with another service or
business in many cases.</li>
                        <li>Rights relating to automated decision-making and profiling. We do not use your personal data in this
way.</li>
                    </ul>
                    <p>For more information about our use of your personal data or exercising your rights as outlined above, please
contact us using the details provided in Part 11.</p>
<p>Further information about your rights can also be obtained from the Information Commissioner’s Office or your
local Citizens Advice Bureau.</p>
<p>IIf you have any cause for complaint about our use of your personal data, you have the right to lodge a complaint
with the Information Commissioner’s Office.</p>
                    <p>
                        <strong>5. What Personal Data Do You Collect?</strong>
                    </p>
                    <p>We may collect some or all of the following personal data (this may vary according to your relationship with us:</p>
                    <p>Name;
                        <br> Date of birth;
                        <br> Address;
                        <br> Email address;
                        <br> Telephone number;
                        <br> Business name;
                        <br> Job title;
                        <br> Profession;
                        <br> Payment information;
                        <br> Property Information;</p>
                    <p>Other information as may be required in the performance of a contract relating to your property.</p>
                    <p>Your personal data is not obtained from third parties.</p>
                    <p>
                        <strong>6. How Do You Use My Personal Data?</strong>
                    </p>
                    <p>Under the GDPR, we must always have a lawful basis for using personal data. This may be because the data is
necessary for our performance of a contract with you, because you have consented to our use of your personal
data, or because it is in my legitimate business interests to use it. Your personal data may be used for one of
the following purposes:</p>
                    <ul>
                        <li>Managing your property or estate.</li>
                        <li>In the sale of a property</li>
                        <li>Supplying our professional services to you. Your personal details are required in order for us to enter
into a contract with you.</li>
                        <li>Personalising and tailoring our services for you.</li>
                        <li>Communicating with you. This may include responding to emails or calls from you.</li>
                        <li>Supplying you with information by email and/or post that you have opted-in to (you may unsubscribe or
opt-out at any time by emailing dataprotection@longleasepm.co.uk</li>
                    </ul>
                    <p>With your permission and/or where permitted by law, we may also use your personal data for marketing
purposes, which may include contacting you by email, telephone, text message and/or post with information,
news, and offers on our services. You will not be sent any unlawful marketing or spam. We will always work to
fully protect your rights and comply with our obligations under the GDPR and the Privacy and Electronic
Communications (EC Directive) Regulations 2003, and you will always have the opportunity to opt-out.</p>
                    <p>We do not use automated system for carrying out certain kinds of decision-making or profiling. If at any point
you wish to query any action that we take on the basis of this or wish to request ‘human intervention’ (i.e. have
someone review the action themselves, rather than relying only on the automated method), the GDPR gives you
the right to do so. Please contact us to find out more using the details in Part 11.</p>
                    <p>
                        <strong>7. How Long Will You Keep My Personal Data?</strong>
                    </p>
                    <p>We will not keep your personal data for any longer than is necessary in light of the reasons for which it was first
collected. Your personal data will therefore be kept for the following periods (or, where there is no fixed period,
the following factors will be used to determine how long it is kept):</p>
                    <ul>
                        <li>Data will be held as long as is required for regulatory purposes. We will review the retention period to
coincide with the review of this policy.</li>
                        <li>Where you are a tenant it will be held for the duration of the tenancy and for a minimum of 2 years after
you vacate the property.</li>
                        <li>Where you are a landlord data will be held for the duration of our contract with you and to comply with HMRC requirements after the contract had ended.</li>
                        <li>Where you are a professional client your data will be held in line with our insurers requirement for the
purposes of professional indemnity insurance records.</li>
                        <li>Where you are a leaseholder your data will be kept whilst you own the property. Your name and
address only will be held for a minimum of 2 years after the property is conveyed.</li>
                    </ul>
                    <p>In all other circumstances data will only be held in accordance with statutory or regulatory requirements.</p>
                    <p>
                        <strong>8. How and Where Do You Store or Transfer My Personal Data?</strong>
                    </p>
                    <p>We will only store or transfer your personal data in the UK or EU. This means that it will be fully protected under
the GDPR.</p>
                    <p>
                        <strong>9. Do You Share My Personal Data?</strong>
                    </p>
                    <p>We may sometimes contract with the third parties to supply services to you on our behalf. These may include
payment processing, delivery, but not marketing. In some cases, those third parties may require access to some
or all of your personal data that we hold.</p>
                    <ul>
                        <li>This means if access is required to your property either rented, leasehold or for sale, we may share
your details with contractors in order to enable timely and effective service. We have a data sharing
agreement with the third parties that means your data will not be used for any other purpose than to
enable the effective repair of your property.</li>
                    </ul>
                    <p>If any of your personal data is required by a third party, as described above, we will take steps to ensure that
your personal data is handled safely, securely, and in accordance with your rights, our obligations, and the third
party’s obligations under the law, as described above in Part 8.</p>
                    <p>In some limited circumstances, we may be legally required to share certain personal data, which might include
yours, if we are involved in legal proceedings or complying with legal obligations, a court order, or the
instructions of a government authority.</p>
                    <p>
                        <strong>10. How Can I Access My Personal Data?</strong>
                    </p>
                    <p>If you want to know what personal data we have about you, you can ask us for details of that personal data and
for a copy of it (where any such personal data is held). This is known as a “subject access request”.</p>
                    <p>All subject access requests should be made in writing and sent to the email or postal addresses shown in Part 11.</p>
                    <p>There is not normally any charge for a subject access request. If your request is ‘manifestly unfounded or
excessive’ (for example, if you make repetitive requests) a fee may be charged to cover our administrative costs
in responding.</p>
                    <p>We will respond to your subject access request within 30 days of receiving it. Normally, we aim to provide a
complete response, including a copy of your personal data within that time. In some cases, however, particularly
if your request is more complex, more time may be required up to a maximum of three months from the date we
receive your request. You will be kept fully informed of our progress.</p>
                    <p>
                        <strong>11. How Do I Contact You?</strong>
                    </p>
                    <p>To contact us about anything to do with your personal data and data protection, including to make a subject
access request, please use the following details for the attention of Data Controller:</p>
                    <p>Email address: dataprotection@longleasepm.co.uk
                        <br> Telephone number: 01905 697979
                        <br> Postal Address: 6 Abbey Lane Court, Abbey Lane, Evesham Worcs WR11 4BY</p>
                    <p>
                        <strong>12. Changes to this Privacy Notice</strong>
                    </p>
                    <p>We may change this Privacy Notice from time to time. This may be necessary, for example, if the law changes,
or if we change our business in a way that affects personal data protection.</p>
                    <p>Any changes will be made available on our website www.longleasepm.co.uk.</p>
                </div>
			</div>
		</div>
	</div>

	<?php include_once 'inc/sidebar.php';?>

</div>

<?php include_once 'inc/footer.php';?>