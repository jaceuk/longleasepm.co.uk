<?php include_once 'inc/header.php';?>

<div class="pure-g">

	<div class="pure-u-1 pure-u-md-2-3">
		<div class="pane">
			<div class="block">
				<h1>Contact Us</h1>
				<p>We are always pleased to hear from you whether it be questions, comments, or you wish to discuss how Longlease can manage your block for you.</p>
				<p>You can contact us the following ways:</p>

				<p><strong>Tel:</strong> 01789 333 464</p>

				<p><strong>Email:</strong> <a href="mailto:info@longleasepm.co.uk">info@longleasepm.co.uk</a></p>

				<h2>Longlease Property Management</h2>
				<p><strong>Registered Office:</strong> 2 Merstow Green, Evesham, Worcestershire WR11 4BD</p>
			</div>
		</div>
	</div>

	<?php include_once 'inc/sidebar.php';?>

</div>

<?php include_once 'inc/footer.php';?>