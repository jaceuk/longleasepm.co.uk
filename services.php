<?php include_once 'inc/header.php';?>

<div class="pure-g">

	<div class="pure-u-1 pure-u-md-2-3">
		<div class="pane">
			<div class="block">
				<h1>A dedicated, personal block management service in the West Midlands</h1>
				<p>We pride ourselves on offering block and estate management expertise and a dedicated, personal service that makes managing your block easy.</p>
				<p>Here are some of the services we provide:</p>

				<h2>Financial accounting services</h2>
				<ul>
					<li>Recommending and agreeing an annual service charge budget</li>
					<li>Issuing and collecting service charge notifications and payments</li>
					<li>Answering leaseholder queries on the service charge</li>
					<li>Setting up resident payment schemes for the service charge</li>
					<li>Administering the management company's dedicated bank accounts</li>
					<li>Providing a financial accounting service including arrears collection</li>
					<li>Processing and paying supplier and contractor invoices</li>
					<li>Providing financial information to the directors on a quarterly or more frequent basis if required</li>
					<li>Circulating profit and loss accounts to all leaseholders within six weeks of the year end</li>
					<li>Liaising with the company accountants for preparation and submission of the annual company accounts</li>
					<li>Liaising with the directors on the content of the prepared annual accounts</li>
				</ul>

				<h2>Insurance services</h2>
				<ul>
					<li>Organising buildings insurance </li>
					<li>Arranging periodic revaluation of rebuilding costs for insurance purposes</li>
					<li>Instigating and overseeing major repairs under communal buildings insurance</li>
					<li>Liaising with insurance assessors and the police where applicable</li>
					<li>Arranging directors' and officers' insurance if required</li>
				</ul>

				<h2>Residents and directors communication and meeting services</h2>
				<ul>
					<li>Attending regular meetings each year with directors</li>
					<li>Preparing meeting notes and actioning matters arising where appropriate</li>
					<li>Circulating notes of meetings held to residents and directors of the management company</li>
					<li>Obtaining alternative quotations for services as requested by the directors</li>
					<li>Arranging and chairing (if requested) Annual General Meetings and Extraordinary General Meetings</li>
					<li>Arranging site meetings with residents as required</li>
					<li>Corresponding with directors and responding to residents' queries</li>
					<li>Liaising with the house builder during the initial years of the management company</li>
				</ul>

				<h2>Maintenance services</h2>
				<ul>
					<li>Providing contractors' specifications for the appointment of cleaners and gardeners and the like</li>
					<li>Organising repairs and obtaining quotations for major items of repair and re-decoration</li>
					<li>Liaising with maintenance contractors as and when required</li>
					<li>Providing tendering services to ensure value for money</li>
					<li>Arranging general maintenance</li>
					<li>Carrying out leaseholder consultations on major works</li>
					<li>Ensuring cyclical maintenance is carried out</li>
					<li>Making regular six weekly maintenance inspections</li>
				</ul>

				<h2>Miscellaneous block management services</h2>
				<ul>
					<li>Liaising and taking instructions from the directors where the terms of the lease are not being adhered to by individual residents</li>
					<li>Seeking continuity of the management company with the appointment of new directors as and when required</li>
					<li>Implementing 24 hour emergency service if requested</li>
				</ul>

				<h2>Transparent property management fees</h2>
				<p>We will agree our annual fee with you in advance and our fee will be included in the service charge budget.</p>

				<h2>Modern property management</h2>
				<p>We use specialist block management computer software that enables us to keep comprehensive, detailed and accurate records on your behalf. This gives you the reassurance that your records comply with legislative requirements and that information can be accessed at the touch of a button.</p>

			</div>
		</div>
	</div>

	<?php include_once 'inc/sidebar.php';?>

</div>

<?php include_once 'inc/footer.php';?>